package com.example.conversormoneda;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;
    private EditText txtCantidad;
    private TextView lblConversion;
    private Spinner spnDivisas;

    private int actualPosition;

    private double cantidad;
    private double conversion;
    private double usd = 0.053530;
    private double cad = 0.069930;
    private double eur = 0.048260;
    private double gbp = 0.041120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        spnDivisas = (Spinner) findViewById(R.id.spnDivisas);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        lblConversion = (TextView) findViewById(R.id.lblConversion);

        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_expandable_list_item_1, getResources().getStringArray(R.array.monedas));
        spnDivisas.setAdapter(Adapter);

        spnDivisas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                actualPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Ingrese una cantidad", Toast.LENGTH_SHORT).show();
                }
                else {
                    cantidad = Double.parseDouble(txtCantidad.getText().toString());
                    if(actualPosition == 0){
                        conversion = cantidad * usd;
                        lblConversion.setText("Total: $"+conversion);
                    }
                    if(actualPosition == 1){
                        conversion = cantidad * cad;
                        lblConversion.setText("Total: $"+conversion);
                    }
                    if(actualPosition == 2){
                        conversion = cantidad * eur;
                        lblConversion.setText("Total: €"+conversion);
                    }
                    if(actualPosition == 3) {
                        conversion = cantidad * gbp;
                        lblConversion.setText("Total: £"+conversion);
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCantidad.setText("");
                lblConversion.setText("");
                spnDivisas.setSelection(0);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
