package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    private ReciboNomina recibo;

    private TextView lblNumRecibo;
    private TextView lblNombre;


    private EditText txtRecibo;
    private EditText txtNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtra;

    private RadioButton rdbPuesto1;
    private RadioButton rdbPuesto2;
    private RadioButton rdbPuesto3;

    private TextView lblImpuestoPorc;
    private TextView lblSubtotal;
    private TextView lblImpuesto;
    private TextView lblTotal;

    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotal;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        recibo = new ReciboNomina();

        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombre = findViewById(R.id.lblNombreNomina);

        txtNombre = findViewById(R.id.txtNombreNomina);
        txtRecibo = findViewById(R.id.txtNumRecibo);
        txtHorasNormal = findViewById(R.id.txtHorasTrab);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);

        rdbPuesto1 = findViewById(R.id.rd1);
        rdbPuesto2 = findViewById(R.id.rd2);
        rdbPuesto3 = findViewById(R.id.rd3);

        lblSubtotal = findViewById(R.id.lblSubtotal);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        txtTotal = findViewById(R.id.txtTotal);


        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();
        txtNombre.setText(datos.getString("nombre"));


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtRecibo.getText().toString().matches("") || txtNombre.getText().toString().matches("") || txtHorasNormal.getText().toString().matches("") || txtHorasExtra.getText().toString().matches("")){
                    Toast.makeText(ReciboNominaActivity.this, "Por favor rellene los campos restantes", Toast.LENGTH_SHORT).show();
                }
                else{
                    recibo.setNombre(txtNombre.getText().toString());
                    recibo.setNumRecibo(Integer.parseInt(txtRecibo.getText().toString()));
                    recibo.setHorasTrabNormal(Float.parseFloat(txtHorasNormal.getText().toString()));
                    recibo.setHorasTrabExtras(Float.parseFloat(txtHorasExtra.getText().toString()));
                    if(rdbPuesto1.isChecked()){
                        recibo.setPuesto(1);
                    }
                    else if(rdbPuesto2.isChecked()){
                        recibo.setPuesto(2);
                    }
                    else if(rdbPuesto3.isChecked()){
                        recibo.setPuesto(3);
                    }
                    txtSubtotal.setText(String.valueOf(recibo.calcularSubtotal()));
                    txtImpuesto.setText(String.valueOf(recibo.calcularImpuesto()));
                    txtTotal.setText(String.valueOf(recibo.calcularTotal()));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRecibo.setText("");
                txtHorasNormal.setText("");
                txtHorasExtra.setText("");
                txtSubtotal.setText("");
                txtImpuesto.setText("");
                txtTotal.setText("");
                rdbPuesto1.toggle();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
